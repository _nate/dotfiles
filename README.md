# Dot files - Nate Pisarski
A collection of configuration files that I use on my computer.

# How to use these
Well, here's the thing... This is more of a backup repository than it is an actually useful share. So, unfortunately, you may not be able to pick these up and "run with them", so to say. But they hopefully serve as useful examples for how to do things on your system. My name is 'nate', so you may have to change some "/home/nate/" stuff around.

# License
Use these truly however you want. Public Domain.
